package net.rizon.moo.plugin.commands.why;

import com.google.inject.Inject;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

import net.rizon.moo.Command;
import net.rizon.moo.CommandSource;
import net.rizon.moo.conf.Config;
import net.rizon.moo.irc.Protocol;
import net.rizon.moo.irc.Server;
import net.rizon.moo.irc.ServerManager;
import net.rizon.moo.plugin.commands.commands;

class DNSBLChecker extends Thread
{
	private CommandSource source;

	private InetAddress ip;

	public DNSBLChecker(CommandSource source, final InetAddress ip)
	{
		this.source = source;
		this.ip = ip;
	}

	@Override
	public void run()
	{
		for (final String dnsbl : commands.conf.why.servers)
		{
			try
			{
				String lookup_addr = getLookupHost(ip, dnsbl);
				InetAddress[] res = InetAddress.getAllByName(lookup_addr);
				// We're only interested in the first result
				String type = res[0].getHostAddress();

				source.reply(this.ip.getHostAddress() + " is listed in " + dnsbl + " as type " + type);
			}
			catch (UnknownHostException ex)
			{
			}
		}
	}

	private String getLookupHost(InetAddress ip, String dnsbl) throws UnknownHostException
	{
		StringBuilder sb = new StringBuilder();
		byte[] bytes = ip.getAddress();

		if (ip instanceof Inet6Address)
		{
			// Format:
			//   ABCD:EFGH:IJKL:MNOP:QRST:UVWX:YZAB:CDEF ->
			//   F.E.D.C.B.A.Z.Y.X.W.V.U.T.S.R.Q.P.O.N.M.L.K.J.I.H.G.F.E.D.C.B.A.dnsbl.host
			for (int i = bytes.length - 1; i >= 0; i--)
			{
				int first = bytes[i] & 0xF;
				int second = (bytes[i] >> 4) & 0xF;
				sb.append(String.format("%X.%X.", first, second));
			}
		}
		else
		{
			// Format:
			//   X.Y.Z.W -> W.Z.Y.X.dnsbl.host
			for (int i = bytes.length - 1; i >= 0; i--)
			{
				sb.append(bytes[i] & 0xFF);
				sb.append(".");
			}
		}

		sb.append(dnsbl);
		return sb.toString();
	}
}

public class CommandWhy extends Command
{
	@Inject
	private ServerManager serverManager;

	@Inject
	private Protocol protocol;

	protected static CommandSource command_source;
	public static String host_ip = "", host_host = "";
	public static int requested = 0;

	@Inject
	public CommandWhy(Config conf)
	{
		super("!WHY", "Find why an IP is banned");

		this.requiresChannel(conf.staff_channels);
		this.requiresChannel(conf.oper_channels);
		this.requiresChannel(conf.admin_channels);
	}

	@Override
	public void onHelp(CommandSource source)
	{
		source.notice("Syntax: !WHY <ip/host>");
		source.notice("Finds out why a certain IP is banned. It is looked for in DNSBLs and k/K/d:lines");
	}

	@Override
	public void execute(CommandSource source, String[] params)
	{
		if (params.length <= 1)
		{
			source.reply("Syntax: !WHY <ip/host>");
			return;
		}

		try
		{
			InetAddress addr = InetAddress.getByName(params[1]);
			host_ip = addr.getHostAddress();
			host_host = addr.getHostName();
			Thread t = new DNSBLChecker(source, addr);
			t.start();
		}
		catch (UnknownHostException ex)
		{
			source.reply("Invalid IP or host");
			return;
		}

		requested = 0;
		for (Server s : serverManager.getServers())
			if (s.getSplit() == null && !s.isServices() && !s.isHub())
			{
				protocol.write("STATS", "k", s.getName());
				protocol.write("STATS", "K", s.getName());
				protocol.write("STATS", "d", s.getName());
				requested += 3;
			}

		command_source = source;
	}
}
