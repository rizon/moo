package net.rizon.moo.plugin.tickets;

import lombok.extern.slf4j.Slf4j;
import net.rizon.moo.Moo;
import net.rizon.moo.logging.LoggerUtils;

@Slf4j
class TicketTimer implements Runnable
{
	private TicketChecker c;

	@Override
	public void run()
	{
		if (c != null && c.isAlive())
			return;

		c = new TicketChecker();
		Moo.injector.injectMembers(c);
		LoggerUtils.initThread(log, c);
		c.start();
	}
}