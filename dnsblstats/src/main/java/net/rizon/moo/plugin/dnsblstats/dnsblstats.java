package net.rizon.moo.plugin.dnsblstats;

import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.google.inject.multibindings.Multibinder;
import io.netty.util.concurrent.ScheduledFuture;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import lombok.extern.slf4j.Slf4j;
import net.rizon.moo.Command;
import net.rizon.moo.Message;
import net.rizon.moo.Moo;
import net.rizon.moo.Plugin;
import net.rizon.moo.conf.ConfigurationException;
import net.rizon.moo.events.EventListener;
import net.rizon.moo.events.OnConnect;
import net.rizon.moo.events.OnReload;
import net.rizon.moo.events.OnServerDestroy;
import net.rizon.moo.events.OnServerLink;
import net.rizon.moo.irc.Protocol;
import net.rizon.moo.irc.Server;
import net.rizon.moo.irc.ServerManager;
import net.rizon.moo.plugin.dnsblstats.comparators.CountComparator;
import net.rizon.moo.plugin.dnsblstats.comparators.ServerComparator;
import net.rizon.moo.plugin.dnsblstats.conf.DnsblStatsConfiguration;

@Slf4j
public class dnsblstats extends Plugin implements EventListener
{
	@Inject
	private CommandDnsblStats dnsbl;

	@Inject
	private ServerManager serverManager;

	@Inject
	private Protocol protocol;

	@Inject
	private StatsRequester requester;

	private DnsblStatsConfiguration conf;
	
	private ScheduledFuture requesterFuture;

	private Map<Server, DnsblInfo> infos = new HashMap<>();

	public dnsblstats() throws ConfigurationException, IOException
	{
		super("DNSBL Stats", "Monitors and shows DNSBL hits");
		conf = DnsblStatsConfiguration.load();
	}

	@Override
	public void start() throws Exception
	{
		requesterFuture = Moo.scheduleWithFixedDelay(requester, 1, TimeUnit.MINUTES);
	}

	@Override
	public void stop()
	{
		requesterFuture.cancel(false);
	}

	public DnsblInfo getDnsblInfoFor(Server s)
	{
		DnsblInfo i = infos.get(s);
		if (i == null)
		{
			i = new DnsblInfo();
			infos.put(s, i);
		}
		return i;
	}
	
	@Subscribe
	public void onConnect(OnConnect evt)
	{
		for (Server s : serverManager.getServers())
			protocol.write("STATS", "B", s.getName());
	}

	@Subscribe
	public void onServerLink(OnServerLink evt)
	{
		Server serv = evt.getServer();
		
		/* Be sure dnsbl stats are up to date, prevents long splits from tripping the dnsbl monitor */
		protocol.write("STATS", "B", serv.getName());
	}

	@Subscribe
	public void onServerDestroy(OnServerDestroy evt)
	{
		Server serv = evt.getServer();
		
		infos.remove(serv);
	}

	@Subscribe
	public void onReload(OnReload evt)
	{
		try
		{
			conf = DnsblStatsConfiguration.load();

			// Now the Guice graph gets rebuilt which reinjects conf everywhere, and then
			// re starts the plugin, which applies the configuration
		}
		catch (Exception ex)
		{
			evt.getSource().reply("Error reloading dnblstats configuration: " + ex.getMessage());

			log.warn("Unable to reload dnsblstats configuration", ex);
		}
	}

	@Override
	public List<Command> getCommands()
	{
		return Arrays.<Command>asList(dnsbl);
	}

	@Override
	protected void configure()
	{
		bind(dnsblstats.class).toInstance(this);

		bind(DnsblStatsConfiguration.class).toInstance(conf);

		bind(StatsRequester.class);

		bind(CountComparator.class);
		bind(ServerComparator.class);

		Multibinder<Command> commandBinder = Multibinder.newSetBinder(binder(), Command.class);
		commandBinder.addBinding().to(CommandDnsblStats.class);

		Multibinder<Message> messageBinder = Multibinder.newSetBinder(binder(), Message.class);
		messageBinder.addBinding().to(Numeric219.class);
		messageBinder.addBinding().to(Numeric227.class);

		Multibinder<EventListener> eventListenerBinder = Multibinder.newSetBinder(binder(), EventListener.class);
		eventListenerBinder.addBinding().toInstance(this);
	}
}
