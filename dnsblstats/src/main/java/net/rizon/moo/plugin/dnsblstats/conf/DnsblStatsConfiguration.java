package net.rizon.moo.plugin.dnsblstats.conf;

import java.io.IOException;
import net.rizon.moo.conf.Configuration;
import net.rizon.moo.conf.ConfigurationException;
import net.rizon.moo.conf.Validator;

public class DnsblStatsConfiguration extends Configuration
{
	public int globalThreshold;
	public int serverThreshold;

	public static DnsblStatsConfiguration load() throws ConfigurationException, IOException
	{
		return load("dnsblstats.yml", DnsblStatsConfiguration.class);
	}

	@Override
	public void validate() throws ConfigurationException
	{
		Validator.validateNotZero("globalThreshold", globalThreshold);
		Validator.validateNotZero("serverThreshold", serverThreshold);
	}
}
